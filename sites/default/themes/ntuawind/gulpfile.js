'use strict';

var shell = require('gulp-shell');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var livereload = require('gulp-livereload');
var cp = require('child_process');

var drush_path = 'drush';

gulp.task('sass:prod', function () {
  gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
       browsers: ['last 2 version']
    }))
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:dev', function () {
  gulp.src('./scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 version']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./css'))
    .pipe(livereload());
});

// run drush to clear the theme registry.
gulp.task('drush', function (done) {
  return cp.spawn('drush', ['cc', 'css-js'], {stdio: 'inherit'})
  .on('close', done);
});

gulp.task('sass:watch', function () {
  livereload.listen();
  gulp.watch('./scss/**/*.scss', ['sass:dev', 'drush']);
});

gulp.task('default', ['sass:dev', 'sass:watch']);
